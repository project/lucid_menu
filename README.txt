
Lucid menu is a highly customizable module which replaces the current menus with new ones. Its idea was heavily influenced by the wonderful nice_menus but attempts to provide more customization options, it is also compatible with drupal 5. Almost every aspect of this module is customizable. It uses the doIMenu 1.5.3 by Donna Iwan Setiawan

1. Installation
   Simply enable the module
   
2. Configuration
   Visit "Administer"->"Site configuration"->"Lucid menu" to configure the global horizontal/vertical menu options
   
3. A new block will be created for every menu block, enable the block in place of the original one and enjoy! You can configure the block for block specific options.

4. Visit the docs folder for a description of every setting that can be customized in this module, almost all aspects are customizable!

5. Visit the demo folder for the original demo that comes with the doIMenu 1.5.3 menu

6. For power users, add any necessary javascript to the lucid_menu_custom.js file in the js folder and it will be included automatically.

Please use the project's page to report any bugs, suggestions or support requests.

For paid customization of the module or drupal consultation, please contact me at aamayreh@gmail.com